(ns samy-reads-api.convert.complete
  (:require
   [ring.util.response :refer [response]]
   [clojure.java.io :as io]
   [clojure.string :as string]
   [samy-reads-api.convert.common :refer [build-file-name file-exists? file-id-not-found]]
   [pantomime.extract :as extract]))

(defn- build-lines [lines]
  (->> lines (filter (fn [line] (not= line " "))) (map (fn [line] (string/split line #" ")))))

(defn- build-response [{:keys [text]}]
  (let [lines (->> text (string/split-lines) (build-lines))]
    {:lines lines}))

(defn- parse-file [file-name]
  (let [parsed-content (extract/parse file-name)]
    (io/delete-file file-name)
    parsed-content))

(defn- convert-file [file-id] (-> file-id (build-file-name) (parse-file) (build-response) (response)))

(defn complete [{{:keys [file-id]} :route-params}]
  (if (not (file-exists? file-id)) (file-id-not-found) (convert-file file-id)))
