(ns samy-reads-api.convert.initiate
  (:require
   [ring.util.response :refer [response]]
   [samy-reads-api.convert.common :refer [build-file-name directory-exists? make-directory]]))

(defn- generate-uuid [] (str (java.util.UUID/randomUUID)))

(defn- write-file [file-name] (spit file-name ""))

(defn initiate [] (let [file-id (generate-uuid) file-name (build-file-name file-id)]
                    (if (directory-exists?) (write-file file-name) ((make-directory) (write-file file-name)))
                    (response {:file_id file-id})))
