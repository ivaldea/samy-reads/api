(ns samy-reads-api.convert.chunks
  (:require [ring.util.response :refer [response]]
            [samy-reads-api.convert.common :refer [build-file-name file-exists? file-id-not-found]]
            [samy-reads-api.common.write-http-object-to-file :refer [write-http-input-to-file]]))

(defn- save-chunk [body file-id]
  (-> file-id (build-file-name) (write-http-input-to-file body))
  (response {:ok true}))

(defn chunks [{:keys [body] {:keys [file-id]} :route-params}]
  (if (not (file-exists? file-id)) (file-id-not-found) (save-chunk body file-id)))
