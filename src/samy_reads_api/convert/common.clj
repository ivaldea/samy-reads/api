(ns samy-reads-api.convert.common
  (:require [clojure.java.io :as io]))

(def ^:private OUTPUT_DIR "/tmp/samy-reads")

(defn build-file-name [file-id] (str OUTPUT_DIR "/" file-id))

(defn file-exists? [file-id] (-> file-id (build-file-name) (io/file) (.exists)))

(defn directory-exists? [] (-> OUTPUT_DIR (io/file) (.isDirectory) (boolean)))

(defn make-directory [] (-> OUTPUT_DIR (io/file) (.mkdirs)))

(defn file-id-not-found [] {:status 404 :body {:ok false :message "File id doesn't exist"}})
