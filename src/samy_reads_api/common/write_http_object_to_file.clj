(ns samy-reads-api.common.write-http-object-to-file
  (:require [clojure.java.io :as io]))

(defn write-http-input-to-file [file-path http-input]
  (with-open [input-stream (io/input-stream http-input)
              output-stream (io/output-stream (io/file file-path))]
    (io/copy input-stream output-stream)))
