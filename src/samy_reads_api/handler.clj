(ns samy-reads-api.handler
  (:require [compojure.core :refer [PUT POST defroutes]]
            [compojure.route :as route]
            [samy-reads-api.convert.initiate :refer [initiate]]
            [samy-reads-api.convert.chunks :refer [chunks]]
            [samy-reads-api.convert.complete :refer [complete]]
            [samy-reads-api.not-found :refer [not-found]]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :as json]
            [ring.middleware.cors :as cors]))

(defroutes app-routes
  (POST "/api/convert/initiate" [] (initiate))
  (PUT "/api/convert/:file-id/chunks" request (chunks request))
  (POST "/api/convert/:file-id/complete" request (complete request))
  (route/not-found (not-found)))

#_{:clj-kondo/ignore [:clojure-lsp/unused-public-var]}
(def app (->
          app-routes
          (json/wrap-json-body {:keywords? true :bigdecimals? true})
          (json/wrap-json-response)
          (cors/wrap-cors :access-control-allow-origin [#".*"] :access-control-allow-methods [:post :put])
          (wrap-defaults api-defaults)))
