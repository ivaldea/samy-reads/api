(ns samy-reads-api.not-found
  (:require [ring.util.response :refer [response]]))

(defn not-found []
  (response {:message "Not Found"}))


